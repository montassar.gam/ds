FROM openjdk:7
WORKDIR /ds-main
COPY ./target .
COPY ./ run.sh
RUN ["chmod", "+777", "./run.sh"]
EXPOSE 8080
ENTRYPOINT ["sh","ds-main/run.sh"]
CMD ["./run.sh"]

